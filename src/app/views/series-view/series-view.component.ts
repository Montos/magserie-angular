import {Component, OnInit} from '@angular/core';
import {Serie} from "../../models/serie.model";
import {SerieService} from "../../services/serie/serie.service";
import {Router} from "@angular/router";
import {CommentaireService} from "../../services/commentaire/commentaire.service";
import {Commentaire} from "../../models/commentaire.model";

@Component({
  selector: 'app-series-view',
  templateUrl: './series-view.component.html',
  styleUrls: ['./series-view.component.css']
})
export class SeriesViewComponent implements OnInit {

  series: Array<Serie>;
  commentaires: Array<Commentaire>;

  constructor(private serieService: SerieService,
              private commentaireService: CommentaireService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.series = this.serieService.series;
  }

  onClickShowSerie(serieId): void {
    this.router.navigate(['serie', serieId]);
  }

  onClickEditSerie(serieId): void {
    this.router.navigate(['serie', 'edit', serieId]);
  }

  onClickDeleteSerie(serieId): void {
    this.serieService.deleteSerie(+serieId);
  }

}
