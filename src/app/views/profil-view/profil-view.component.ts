import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../models/user.model";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-profil-view',
  templateUrl: './profil-view.component.html',
  styleUrls: ['./profil-view.component.css']
})
export class ProfilViewComponent implements OnInit {

  user: User;

  editProfilForm: FormGroup

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.user = this.userService.user;

    this.editProfilForm = this.formBuilder.group({
      firstname: [this.user.name, Validators.required],
      password: [this.user.password, Validators.required]
    });
  }

  onSubmitEditProfil(): void {
    this.user.name = this.editProfilForm.value.firstname;
    this.user.password = this.editProfilForm.value.password;

    this.userService.editUser(this.user);

    this.router.navigate(['series']);
  }

}
