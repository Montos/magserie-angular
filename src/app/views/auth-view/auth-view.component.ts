import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css']
})
export class AuthViewComponent implements OnInit {

  errorMsg: string;

  authForm: FormGroup;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {

    this.authForm = this.formBuilder.group({
      nom: ['', [Validators.required]],
      password: ['', Validators.required]
    });

  }

  onSignIn() {

    this.authService
      .signIn(this.authForm.value.nom, this.authForm.value.password)
      .then(() => {
        this.router.navigate(['series']);
      })
      .catch((err) => {
        this.errorMsg = err;
      })

  }

}
