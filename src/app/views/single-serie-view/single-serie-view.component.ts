import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SerieService} from "../../services/serie/serie.service";
import {Serie} from "../../models/serie.model";

@Component({
  selector: 'app-single-serie-view',
  templateUrl: './single-serie-view.component.html',
  styleUrls: ['./single-serie-view.component.css']
})
export class SingleSerieViewComponent implements OnInit {

  serie: Serie;

  constructor(private route: ActivatedRoute,
              private serieService: SerieService,
              private router: Router) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.serie = this.serieService.getSerieById(+id);
  }

  clickToAccordion() {
    const acc = document.getElementsByClassName("accordion");
    for (let i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        let panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
  }

  onClickNewComment(serieid): void {
    this.router.navigate(['commentaire', 'new', serieid]);
  }

}
