import {Component, OnInit} from '@angular/core';
import {SerieService} from "../../services/serie/serie.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Serie} from "../../models/serie.model";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-edit-serie-view',
  templateUrl: './edit-serie-view.component.html',
  styleUrls: ['./edit-serie-view.component.css']
})
export class EditSerieViewComponent implements OnInit {

  serie: Serie;
  editSerieForm: FormGroup

  constructor(private serieService: SerieService, private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    const pipe = new DatePipe('en-US');
    const id = this.route.snapshot.params.id;

    this.serie = this.serieService.getSerieById(+id);

    this.editSerieForm = this.formBuilder.group({
      title: [this.serie.nom, Validators.required],
      //dateSortie: [this.serie.dateSortie, Validators.required],
      dateSortie: [pipe.transform(this.serie.dateSortie, 'dd/MM/yyyy')],
      nbsaison: [this.serie.nbSaisons],
      desc: [this.serie.desc, [Validators.required, Validators.maxLength(256)]],
      critique: [this.serie.critique, Validators.maxLength(256)],
      linkimg: [this.serie.photo_link]
    });
  }

  onSubmitEditSerie(): void {
    this.serie.nom = this.editSerieForm.value.title;
    this.serie.dateSortie = this.editSerieForm.value.dateSortie;
    this.serie.nbSaisons = this.editSerieForm.value.nbsaison;
    this.serie.desc = this.editSerieForm.value.desc;
    this.serie.critique = this.editSerieForm.value.critique;
    this.serie.photo_link = this.editSerieForm.value.linkimg;

    this.serieService.editSerie(this.serie);
  }
}
