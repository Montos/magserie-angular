import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCommentaireComponent } from './new-commentaire.component';

describe('NewCommentaireComponent', () => {
  let component: NewCommentaireComponent;
  let fixture: ComponentFixture<NewCommentaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCommentaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCommentaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
