import {Component, Input, OnInit, Output} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Serie} from "../../models/serie.model";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../models/user.model";
import {UserService} from "../../services/user/user.service";
import {SerieService} from "../../services/serie/serie.service";
import {Commentaire} from "../../models/commentaire.model";
import {CommentaireService} from "../../services/commentaire/commentaire.service";

@Component({
  selector: 'app-new-commentaire',
  templateUrl: './new-commentaire.component.html',
  styleUrls: ['./new-commentaire.component.css']
})
export class NewCommentaireComponent implements OnInit {
  newCommentaireForm: FormGroup
  user: User;
  serie: Serie;

  serieId: number;

  constructor(private authService: AuthService, private formBuilder: FormBuilder,
              private router: Router, private userService: UserService,
              private serieService: SerieService, private commService: CommentaireService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // Retrieve serie id from the current route
    this.serieId = parseInt(this.route.snapshot.params.sid);

    this.newCommentaireForm = this.formBuilder.group({
      commentaire: ['', Validators.required]
    });
  }

  OnClickreturnSerieInfo() {
    this.router.navigate(['serie', this.serieId]);
  }

  onSubmitNewCommentaire(): void {
    this.serie = this.serieService.getSerieById(this.serieId);

    const newComm = new Commentaire(new Date(Date.now()), this.userService.user, this.serie, this.newCommentaireForm.value.commentaire);
    this.commService.addCommentaire(newComm);

    this.serie.commentaires.push(newComm);
    this.serieService.editSerie(this.serie);

    this.router.navigate(['serie', this.serieId]);
  }

}

