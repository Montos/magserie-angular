import {Component, OnInit} from '@angular/core';
import {Serie} from "../../models/serie.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SerieService} from "../../services/serie/serie.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-serie-view',
  templateUrl: './new-serie-view.component.html',
  styleUrls: ['./new-serie-view.component.css']
})
export class NewSerieViewComponent implements OnInit {

  newSerieForm: FormGroup

  constructor(private serieService: SerieService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.newSerieForm = this.formBuilder.group({
      title: ['', Validators.required],
      dateSortie: ['', Validators.required],
      nbsaison: [''],
      desc: ['', [Validators.required, Validators.maxLength(256)]],
      critique: [''],
      linkimg: ['']
    });
  }

  onSubmitNewSerie(): void {
    //new Serie(nom,nbSaisons,desc,critique,photo_link,dateSortie)
    const newSerie = new Serie(
      this.newSerieForm.value.title,
      this.newSerieForm.value.nbsaison,
      this.newSerieForm.value.desc,
      this.newSerieForm.value.critique,
      this.newSerieForm.value.linkimg,
      this.newSerieForm.value.dateSortie
    );

    this.serieService.addSerie(newSerie);
    this.router.navigate(['series']);
  }
}
