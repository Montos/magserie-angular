import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthViewComponent} from "./views/auth-view/auth-view.component";
import {AuthGuardService} from "./services/guards/auth-guard/auth-guard.service";
import {SeriesViewComponent} from "./views/series-view/series-view.component";
import {SingleSerieViewComponent} from "./views/single-serie-view/single-serie-view.component";
import {EditSerieViewComponent} from "./views/edit-serie-view/edit-serie-view.component";
import {NewSerieViewComponent} from "./views/new-serie-view/new-serie-view.component";
import {ErrorViewComponent} from "./views/error-view/error-view.component";
import {ProfilViewComponent} from "./views/profil-view/profil-view.component";
import {NewCommentaireComponent} from"./views/new-commentaire-view/new-commentaire.component"


const routes: Routes = [
  { path: 'auth', component: AuthViewComponent},
  { path: 'series', canActivate: [AuthGuardService], component: SeriesViewComponent},
  { path: '', canActivate: [AuthGuardService], component: SeriesViewComponent},
  { path: 'serie/new', canActivate: [AuthGuardService], component: NewSerieViewComponent},
  { path: 'serie/edit/:id', canActivate: [AuthGuardService], component: EditSerieViewComponent},
  { path: 'serie/:id', canActivate: [AuthGuardService], component: SingleSerieViewComponent},
  {path: 'commentaire/new/:sid',canActivate:[AuthGuardService],component: NewCommentaireComponent},
  { path: 'profil', canActivate: [AuthGuardService], component: ProfilViewComponent},
  { path: 'not-found', component: ErrorViewComponent},
  { path: '**', redirectTo: 'not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
