import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthViewComponent} from './views/auth-view/auth-view.component';
import {SeriesViewComponent} from './views/series-view/series-view.component';
import {SingleSerieViewComponent} from './views/single-serie-view/single-serie-view.component';
import {ErrorViewComponent} from './views/error-view/error-view.component';
import {HeaderComponent} from './components/header/header.component';
import {ProfilViewComponent} from './views/profil-view/profil-view.component';
import {NewSerieViewComponent} from './views/new-serie-view/new-serie-view.component';
import {EditSerieViewComponent} from './views/edit-serie-view/edit-serie-view.component';
import {UserService} from "./services/user/user.service";
import {SerieService} from "./services/serie/serie.service";
import {AuthService} from "./services/auth/auth.service";
import {FooterComponent} from './components/footer/footer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {NewCommentaireComponent} from './views/new-commentaire-view/new-commentaire.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthViewComponent,
    SeriesViewComponent,
    SingleSerieViewComponent,
    ErrorViewComponent,
    HeaderComponent,
    ProfilViewComponent,
    NewSerieViewComponent,
    EditSerieViewComponent,
    FooterComponent,
    NewCommentaireComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSliderModule
  ],
  providers: [UserService, SerieService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
