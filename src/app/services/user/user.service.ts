import {Injectable} from '@angular/core';
import {User} from "../../models/user.model";
import {error} from "@angular/compiler/src/util";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  users: Array<User>;

  constructor() {
    //this.user = new User('Administrateur', 'azerty');
    this.user = new User('Administrateur', 'azerty');

    this.users = [];
    this.users.push(this.user);
    this.users.push(new User("John", "aaa"));
    this.users.push(new User("Mark", "aaa"));
    this.users.push(new User("Axel", "aaa"));
    this.users.push(new User("Lina", "aaa"));
    this.users.push(new User("Pixelle", "aaa"));

  }

  /**
   * Method for quickly edit the current user
   * @param editedUser
   */
  editUser(editedUser: User): void {
    this.user = editedUser;
  }

  getUserById(id) {
    for (let u of this.users) {
      if (u.id === id) {
        return u;
      }//else console.error()

    }
  }


}
