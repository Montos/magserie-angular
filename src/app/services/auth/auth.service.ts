import {Injectable} from '@angular/core';
import {UserService} from "../user/user.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuth = false;

  constructor(private userService: UserService) {
  }

  /**
   * Method for connect the user
   * @param name
   * @param password
   */
  signIn(name: string, password: string) {

    return new Promise((res, rej) => {

      setTimeout(() => {
        if (this.userService.user.name === name
          && this.userService.user.password === password) {
          this.isAuth = true;
          res();
        } else {
          rej('Identification incorrecte !!!');
        }
      }, 1000);
    });
  }

  /**
   * Method for disconnect the user
   */
  signOut() {
    return new Promise((res) => {
      setTimeout(() => {
        this.isAuth = false;
        res();
      }, 1000);
    });
  }
}
