import {Injectable} from '@angular/core';
import {Commentaire} from "../../models/commentaire.model";
import {SerieService} from '../serie/serie.service'
import {UserService} from "../user/user.service";
import {User} from "../../models/user.model";
import {Serie} from "../../models/serie.model";

@Injectable({
  providedIn: 'root'
})
export class CommentaireService {
  comm: Commentaire;
  commentaires: Array<Commentaire>;
  user: User;
  serie: Serie;

  constructor(private serieService: SerieService, private userService: UserService) {
    this.commentaires = [];

    for (let i = 0; i < 50; i++) {
      this.user = this.userService.users[Math.floor(Math.random() * this.userService.users.length)];
      this.serie = this.serieService.series[Math.floor(Math.random() * this.serieService.series.length)];
      this.comm = new Commentaire(new Date(Date.now()), this.user, this.serie, "commentaire N-" + i);

      this.commentaires.push(this.comm);

      this.comm.id = this.serie.commentaires.length + 1;

      this.serie.commentaires.push(this.comm);
    }
  }


  /**
   * add comment to list of comments and liste of serie affected by
   * @param comm
   */
  addCommentaire(comm) {
    this.commentaires.push(comm);
  }
}
