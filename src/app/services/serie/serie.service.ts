import {Injectable} from '@angular/core';
import {Serie} from "../../models/serie.model";
import {errorObject} from "rxjs/internal-compatibility";

@Injectable({
  providedIn: 'root'
})
export class SerieService {

  series: Array<Serie>;

  constructor() {
    this.series = [];
    this.series.push(new Serie(
      'Cursed : La rebelle', 3, 'Serie aventure d\'une rebelle', 3,
      'http://fr.web.img6.acsta.net/r_1920_1080/pictures/20/07/27/11/48/3432043.jpg', null
    ));
    this.series.push(new Serie('Umbrella Academy', 2, 'Histoire d\'un parapluie qui fait greve de la faim', 5,
      'http://larubriquedelou.fr/wp-content/uploads/2019/03/umbrella-academy-header.jpg', null
    ));
    this.series.push(new Serie('Dark', 4, 'Une nuit sombre sans lune', 4,
      'https://static0.srcdn.com/wordpress/wp-content/uploads/2019/06/Netflix-Dark-Season-3.jpg', null
    ));
    this.series.push(new Serie('La Casa de Papel', 6, 'Case a popo ou case a papi', 5,
      'https://fhm.nl/wp-content/uploads/2018/09/La-Casa-de-Papel-seizoen-3.jpg', null
    ));
    this.series.push(new Serie('Lucifer', 6, 'Le demon qui se cache deriere un visage d\'ange', 5,
      'https://image.tmdb.org/t/p/original/zXcGhteIjRH2qjOpwYgLSpVri8P.jpg', null
    ));
    this.series.push(new Serie('Peaky Blinders', 2, 'A suivre pour comprendre', 4,
      'https://meplustv.files.wordpress.com/2014/11/peaky-blinders-cast.jpg', null
    ));
    this.series.push(new Serie('The Last Kingdom', 6, 'Le dernier royaume', 3,
      'https://serieophile.fr/wp-content/uploads/2020/04/the-last-kingdom-saison-4.jpg', null
    ));
    this.series.push(new Serie('Stranger Things', 6, 'Etrange Etrange !!!', 4,
      'https://i.ytimg.com/vi/SNHWpQ0-CdY/maxresdefault.jpg', null
    ));

    /**
     * add comment to each
     */
    //this.series.forEach((s)=>s.commentaires.push('commentaires test pour verifier '))

  }

  /**
   * Method for retrieve a serie by id
   * @param sId
   */
  getSerieById(sId) {
    for (let serie of this.series) {
      if (serie.id === sId) {
        return serie;
      } else console.assert(errorObject);
    }
  }

  /**
   * Method for add a new serie on the series array
   * @param newSerie
   */
  addSerie(newSerie: Serie) {
    this.series.push(newSerie);
  }

  /**
   * Method for edit a serie on the series array
   * @param editS
   */
  editSerie(editS: Serie) {
    for (let i = 0; i < this.series.length - 1; i++) {
      if (this.series[i].id === editS.id) {
        this.series[i] = editS;
        break;
      }
    }
  }

  /**
   * Method for delete a serie on the series array
   * @param sId
   */
  deleteSerie(sId: number) {
    for (let i = 0; i < this.series.length; i++) {
      if (sId === this.series[i].id) {
        //if(this.series[i] === this.getSerieById(sId)){
        this.series.splice(i, 1);
        break;
      }
    }
  }

}
