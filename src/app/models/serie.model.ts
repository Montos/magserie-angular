import {Commentaire} from "./commentaire.model";

export class Serie {

  private readonly _id: number;
  private _nom: string;
  private _dateSortie: Date;
  private _nbSaisons: number;
  private _desc: string;
  private _critique: number;
  private _photo_link: string;
  private _commentaires: Array<Commentaire>;

  private static _nbId = 1;

  constructor(nom: string, nbSaisons: number, desc: string, critique: number, photo_link: string, dateSortie: Date) {
    this._id = Serie._nbId++;
    this._nom = nom;
    this._dateSortie = new Date() || dateSortie;
    this._nbSaisons = nbSaisons;
    this._desc = desc;
    this._critique = critique;
    this._photo_link = photo_link;
    this._commentaires = [];
  }

  get id(): number {
    return this._id;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get dateSortie(): Date {
    return this._dateSortie;
  }

  set dateSortie(value: Date) {
    this._dateSortie = value;
  }

  get nbSaisons(): number {
    return this._nbSaisons;
  }

  set nbSaisons(value: number) {
    this._nbSaisons = value;
  }

  get desc(): string {
    return this._desc;
  }

  set desc(value: string) {
    this._desc = value;
  }

  get critique(): number {
    return this._critique;
  }

  set critique(value: number) {
    this._critique = value;
  }

  get photo_link(): string {
    return this._photo_link;
  }

  set photo_link(value: string) {
    this._photo_link = value;
  }

  get commentaires(): Array<Commentaire> {
    return this._commentaires;
  }

  set commentaires(value: Array<Commentaire>) {
    this._commentaires = value;
  }
}
