import {User} from "./user.model";
import {Serie} from "./serie.model";

export class Commentaire {
  private _id: number;
  private _datecrea: Date;
  private _auteur: User;
  private _comm: string;
  private _serie: Serie;

  private static _nbId = 1;

  constructor(datecrea: Date, auteur: User, serie: Serie, comm: string) {
    this._id = Commentaire._nbId++;
    this._datecrea = datecrea;
    this._auteur = auteur;
    this._serie = serie;
    this._comm = comm;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get datecrea(): Date {
    return this._datecrea;
  }

  set datecrea(value: Date) {
    this._datecrea = value;
  }

  get auteur(): User {
    return this._auteur;
  }

  set auteur(value: User) {
    this._auteur = value;
  }

  get serie(): Serie {
    return this._serie;
  }

  set serie(value: Serie) {
    this._serie = value;
  }

  get comm(): string {
    return this._comm;
  }

  set comm(value: string) {
    this._comm = value;
  }
}
