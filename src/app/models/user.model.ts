export class User {
  private readonly _id: number;
  private _name: string;
  private _password: string;

  private static num = 1;

  constructor(name: string, password: string) {
    this._id = User.num++;
    this._name = name;
    this._password = password;
  }


  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }
}
